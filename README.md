# LinkedIN Connections Bot #

This short script allows you to add new connections to your account

** ~WARNING~ ** **This application only runs on MacOS/Linux**

### Getting started ###

In order to start this script, you must first install:
* Node.js => 7.x

After this, you simply need to open your command line and type:

```
$> npm install
```

### How to use it ###

Open your command line and type:

```
$> es connect.js --acc="your account" --pass="your password"
```
--acc and --pass arguments must be filled with your account and password