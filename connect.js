import Nightmare from "nightmare";
const argv = require('yargs').argv;

const app = new Nightmare({
    show: false
});

let count = 0;

//Please insert your account here:
let account = argv.acc;

//Please insert your password here:
let password = argv.pass;

if(account === undefined || password === undefined){
	console.log("Please enter your account and password to continue!");
} else if(account === "" || password === "") {
    console.log("Please fill all of your arguments!")
} else {
	app
	    .goto("https://www.linkedin.com")
	    .type("#login-email", account)
	    .type("#login-password", password)
	    .click("#login-submit")
	    .wait("a[data-alias='relationships']")
	    .click("a[data-alias='relationships']")
	    .wait(".mn-pymk-list__card")
	    .then(() => {
		    console.log("Farmer start!");
		    instanceOne()
	    })
	    .catch((error) => {
            console.log(error);
            start();
	    })
}

function start() {
    app
        .wait("a[data-alias='relationships']")
        .click("a[data-alias='relationships']")
        .wait(".mn-pymk-list__card")
        .then(() => {
            console.log("Farmer start!");
            instanceOne()
        })
        .catch((error) => {
            console.log(error);
            start();
        })

}

function instanceOne() {
    app
        .evaluate(() => {
            window.scrollTo(0,0);

        })
        .wait(".mn-pymk-list__card")
        .click(".mn-pymk-list__card .button-secondary-small")
        .click(".mn-pymk-list__card:nth-child(2) .button-secondary-small")
        .click(".mn-pymk-list__card:nth-child(3) .button-secondary-small")
        .then(() => {
            instanceTwo();
            count = count + 3;
            console.log(count);
        })
        .catch((error) => {
            console.log(error);
            start();
        });
}


function instanceTwo(){
    app
        .evaluate(() => {
            window.scrollTo(0,document.body.scrollHeight);
        })
        .wait(".mn-pymk-list__card")
        .click(".mn-pymk-list__card .button-secondary-small")
        .click(".mn-pymk-list__card:nth-child(2) .button-secondary-small")
        .click(".mn-pymk-list__card:nth-child(3) .button-secondary-small")
        .then(() => {
            instanceOne();
            count = count + 3;
            console.log(count);
        })
        .catch((error) => {
            console.log(error);
            start();
        });
}
